# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# AUTOGENERATED FILE - No need to edit
# GENERATED AT: 2022-04-07T10:13:31"
# Re-create by running 'generate-routes' command
# Run 'generate-routes --help' for more options


def add_routes(config):
    """Add routes to pyramid application.

    :param config: pyramid config file
    :type config: Configurator
    """
    handlers = [
        {
            "route": "/api/v2/document/openapi/{filename}",
            "handler": "apidocs_fileserver",
            "method": "GET",
            "view": "zsnl_document_http.views.apidocs.apidocs_fileserver",
        },
        {
            "route": "/api/v2/document/get_document",
            "handler": "get_document",
            "method": "GET",
            "view": "zsnl_document_http.views.views.get_document",
        },
        {
            "route": "/api/v2/document/create_document",
            "handler": "create_document",
            "method": "POST",
            "view": "zsnl_document_http.views.views.create_document",
        },
        {
            "route": "/api/v2/file/create_file",
            "handler": "create_file",
            "method": "POST",
            "view": "zsnl_document_http.views.views.create_file",
        },
        {
            "route": "/api/v2/document/create_document_from_attachment",
            "handler": "create_document_from_attachment",
            "method": "POST",
            "view": "zsnl_document_http.views.views.create_document_from_attachment",
        },
        {
            "route": "/api/v2/document/create_document_from_message",
            "handler": "create_document_from_message",
            "method": "POST",
            "view": "zsnl_document_http.views.views.create_document_from_message",
        },
        {
            "route": "/api/v2/document/search_document",
            "handler": "search_document",
            "method": "GET",
            "view": "zsnl_document_http.views.views.search_document",
        },
        {
            "route": "/api/v2/document/get_directory_entries_for_case",
            "handler": "get_directory_entries_for_case",
            "method": "GET",
            "view": "zsnl_document_http.views.views.get_directory_entries_for_case",
        },
        {
            "route": "/api/v2/document/create_document_from_file",
            "handler": "create_document_from_file",
            "method": "POST",
            "view": "zsnl_document_http.views.views.create_document_from_file",
        },
        {
            "route": "/api/v2/document/download_document",
            "handler": "download_document",
            "method": "GET",
            "view": "zsnl_document_http.views.views.download_document",
        },
        {
            "route": "/api/v2/document/get_directory_entries_for_intake",
            "handler": "get_directory_entries_for_intake",
            "method": "GET",
            "view": "zsnl_document_http.views.views.get_directory_entries_for_intake",
        },
        {
            "route": "/api/v2/document/preview_document",
            "handler": "preview_document",
            "method": "GET",
            "view": "zsnl_document_http.views.views.preview_document",
        },
        {
            "route": "/api/v2/document/get_document_labels",
            "handler": "get_document_labels",
            "method": "GET",
            "view": "zsnl_document_http.views.document_label.DocumentLabelViews",
            "viewclass": "DocumentLabelViews",
        },
        {
            "route": "/api/v2/document/add_document_to_case",
            "handler": "add_document_to_case",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/update_document",
            "handler": "update_document",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/delete_document",
            "handler": "delete_document",
            "method": "POST",
            "view": "zsnl_document_http.views.views.delete_document",
        },
        {
            "route": "/api/v2/document/reject_assigned_document",
            "handler": "reject_assigned_document",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/assign_document_to_user",
            "handler": "assign_document_to_user",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/assign_document_to_role",
            "handler": "assign_document_to_role",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/edit_document_online",
            "handler": "edit_document_online",
            "method": "GET",
            "view": "zsnl_document_http.views.views.edit_document_online",
        },
        {
            "route": "/api/v2/document/update_document_from_external_editor",
            "handler": "update_document_from_external_editor",
            "method": "POST",
            "view": "zsnl_document_http.views.views.update_document_from_external_editor",
        },
        {
            "route": "/api/v2/document/create_directory",
            "handler": "create_directory",
            "method": "POST",
            "view": "zsnl_document_http.views.views.create_directory",
        },
        {
            "route": "/api/v2/document/thumbnail_document",
            "handler": "thumbnail_document",
            "method": "GET",
            "view": "zsnl_document_http.views.views.thumbnail_document",
        },
        {
            "route": "/api/v2/document/apply_labels",
            "handler": "apply_labels_to_document",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/remove_labels",
            "handler": "remove_labels_from_document",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/move_to_directory",
            "handler": "move_to_directory",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
        {
            "route": "/api/v2/document/accept_document",
            "handler": "accept_document",
            "method": "POST",
            "view": "zsnl_document_http.views.document.DocumentViews",
            "viewclass": "DocumentViews",
        },
    ]

    for h in handlers:
        config.add_route(
            name=h["handler"], pattern=h["route"], request_method=h["method"]
        )
        config.add_view(
            view=h["view"],
            route_name=h["handler"],
            request_method=h["method"],
        )
