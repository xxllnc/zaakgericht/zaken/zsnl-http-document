#! /bin/sh

# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

set -e

while getopts "fcj:" opt; do
  case "${opt}" in
    f)
      fix_errors=1
      ;;
    c)
      test_coverage=1
      ;;
    j)
      junit_file="${OPTARG}"
      ;;
    \?)
      echo "Usage: $0 [-f] [-c] [-j=filename]"
      ;;
  esac
done


PYTESTARGS=""
if [ -n "$test_coverage" ]; then
  PYTESTARGS="$PYTESTARGS --cov-report=xml --cov-report=term --cov-branch --cov=."
fi

if [ -n "$junit_file" ]; then
  PYTESTARGS="$PYTESTARGS --junitxml=$junit_file"
fi

ISORTARGS="-sg .eggs -sg .env -m 3 -tc -rc -ds"
BLACKARGS='-l 79 --exclude \\.eggs\|\\.env\|\\.git'

if [ -z "$fix_errors" ]; then
    ISORTARGS="$ISORTARGS --check-only"
    BLACKARGS="$BLACKARGS --check"
fi

FLAKEARGS="--exclude=.svn,CVS,.bzr,.hg,.git,__pycache__,.tox,.eggs,*.egg,.env"
MYPYARGS="--ignore-missing-imports --follow-imports=silent --show-column-numbers"

read -r COMMANDS <<EOF
    pytest $PYTESTARGS && \
    echo 'Run isort $ISORTARGS' && \
    isort $ISORTARGS . && \
    echo 'Run black $BLACKARGS' && \
    black $BLACKARGS . && \
    echo 'Run flake8 $FLAKEARGS' && \
    flake8 $FLAKEARGS && \
    echo 'Run mypy $MYPYARGS [disabled]' && \
    echo mypy $MYPYARGS . && \
    pip-audit && \
    liccheck -s strategy.ini -r <(grep -F -v 'git+https' requirements/base.txt)
EOF

if [ -f .run-git-hooks-via-compose ]; then
  echo "$COMMANDS" | docker-compose run -T "${PWD##*/}" bash
else
  echo "$COMMANDS" | bash
fi
